package com.example.routes

import com.example.model.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

fun Route.userRoute(){
    route("register") {
        post {
            val user = call.receive<User>()
            uploadUser()
            if (userTable.contains(user.username)) {
                call.respondText("L'usuari ja existeix", status = HttpStatusCode.Conflict)
                return@post
            } else {
                userTable[user.username] = getMd5Digest("${user.username}:$myRealm:${user.password}")
                userList = loadUsersFromFile("src/main/kotlin/com/example/resources/userJson.json")
                userList.add(user)
                updateFile("src/main/kotlin/com/example/resources/userJson.json", userList)
                call.respondText("Usuari registrat correctament", status = HttpStatusCode.Accepted)
            }
        }
    }
    route("login") {
        post{
            val user = call.receive<User>()
            userTable = uploadUser()
            val userHidden = getMd5Digest("${user.username}:$myRealm:${user.password}")
            println(userTable[user.username])
            println(userHidden)
            if (userTable.containsKey(user.username) && userTable[user.username]?.contentEquals(userHidden) == true) {
                call.respondText("Login correcte", status = HttpStatusCode.Accepted)
                return@post
            } else {
                call.respondText("Login incorrecte", status = HttpStatusCode.Conflict)
            }

        }
    }
}
fun loadUsersFromFile(FileRoute: String): MutableList<User> {
    val json = Json { ignoreUnknownKeys = true }
    val fileContent = File(FileRoute).readText()
    userList = json.decodeFromString<MutableList<User>>(fileContent)
    return userList
}

fun updateFile(FileRoute: String, ObjectList: MutableList<User>){
    val json = Json { ignoreUnknownKeys = true }
    val jsonString = json.encodeToString(ObjectList)
    File(FileRoute).writeText(jsonString)
}


