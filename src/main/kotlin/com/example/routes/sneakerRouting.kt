package com.example.routes

import com.example.model.Sneaker
import com.example.model.sneakersStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*


fun Route.sneakerRouting(){
    authenticate("myAuth"){
        route("sneakers") {
            get {
                if (sneakersStorage.isNotEmpty()) call.respond(sneakersStorage)
                else call.respondText("No sneakers found.", status = HttpStatusCode.OK)
            }
            get("{marca?}") {
                if (call.parameters["marca"].isNullOrBlank()) return@get call.respondText(
                    "Missing marca",
                    status = HttpStatusCode.BadRequest
                )
                val marca = call.parameters["marca"]
                for (sneaker in sneakersStorage) {
                    if (sneaker.marca == marca) return@get call.respond(sneaker)
                }
                call.respondText(
                    "Sneaker with marca $marca not found",
                    status = HttpStatusCode.NotFound
                )
            }
            post {
                val customer = call.receive<Sneaker>()
                sneakersStorage.add(customer)
                call.respondText("Sneaker stored correctly", status = HttpStatusCode.Created)
            }
            put("{id?}") {
                if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
                val id = call.parameters["id"]
                val custToUpd = call.receive<Sneaker>()
                for (sneaker in sneakersStorage) {
                    if (sneaker.id == id) {
                        sneaker.foto = custToUpd.foto
                        sneaker.nombre = custToUpd.nombre
                        sneaker.marca = custToUpd.marca
                        sneaker.descripcion = custToUpd.descripcion
                        sneaker.talla = custToUpd.talla
                        sneaker.precio = custToUpd.precio
                        return@put call.respondText(
                            "Sneaker with id $id has been updated",
                            status = HttpStatusCode.Accepted
                        )
                    }
                }
                call.respondText(
                    "Sneaker with id $id not found",
                    status = HttpStatusCode.NotFound
                )
            }
//        post {
//            val data = call.receiveMultipart()
//            data.forEachPart { part ->
//                when (part) {
//                    is PartData.FormItem -> {
//                        if (part.name == "id") {
//                            id = part.value
//                        } else {
//                            name = part.value
//                        }
//                    }
//
//                    is PartData.FileItem -> {
//                        fileName = part.originalFileName as String
//                        var fileBytes = part.streamProvider().readBytes()
//                        File("uploads/$fileName").writeBytes(fileBytes)
//                    }
//
//                    else -> {}
//                }
//            }
//            val user = Sneakers()
//            sneakersStorage.add(user)
//            call.respondText(
//                "Guy stored correctly and \"$fileName is uploaded to 'uploads/$fileName'\"",
//                status = HttpStatusCode.Created
//            )
//        }
            delete("{id?}") {
                if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
                val id = call.parameters["id"]
                for (sneaker in sneakersStorage) {
                    if (sneaker.id == id) {
                        sneakersStorage.remove(sneaker)
                        return@delete call.respondText("Sneaker removed correctly", status = HttpStatusCode.Accepted)
                    }
                }
                call.respondText(
                    "Sneaker with id $id not found",
                    status = HttpStatusCode.NotFound
                )
            }
        }
    }
}

