package com.example.model

import io.ktor.server.auth.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.charset.StandardCharsets.UTF_8
import java.security.MessageDigest

data class CustomPrincipal(val userName: String, val realm: String) : Principal

fun getMd5Digest(str: String): ByteArray = MessageDigest.getInstance("MD5").digest(str.toByteArray(UTF_8))

val myRealm = "Access to the '/' path"
var userTable: MutableMap<String, ByteArray> = mutableMapOf(
    "admin" to getMd5Digest("admin:$myRealm:password")
)

fun uploadUser(): MutableMap<String, ByteArray> {
    val json = Json { ignoreUnknownKeys = true }
    val fileContent = File("src/main/kotlin/com/example/resources/userJson.json").readText()
    userList = json.decodeFromString<MutableList<User>>(fileContent)
    for (i in userList){
        userTable.put(i.username, getMd5Digest("${i.username}:$myRealm:${i.password}"))
    }
    println(userTable)
    return userTable
}

