package com.example.plugins

import com.example.model.CustomPrincipal
import com.example.routes.sneakerRouting
import com.example.routes.userRoute
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*
import io.ktor.server.auth.*

fun Application.configureRouting() {
    routing {
        sneakerRouting()
        userRoute()
    }

}
